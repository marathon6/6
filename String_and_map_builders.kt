import java.util.HashMap

fun <K, V> buildMutableMap(hashMap:HashMap<K, V>.() -> Unit):Map<K, V>{
    val map = HashMap<K, V>()
    map.hashMap()
    return map
}

fun usage(): Map<Int, String> {
    return buildMutableMap {
        put(0, "0")
        for (i in 1..10) {
            put(i, "$i")
        }
    }
}